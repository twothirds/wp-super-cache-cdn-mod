<?php 
/*
Plugin Name: WP Super Cache CDN Mod
Plugin URI: http://twothirdsdesign.co.uk/cdn-super-cache-mod/
Description: Very fast caching plugin for WordPress.
Version: 1.1
Author: Geraint Palmer
Author URI: http://twothirdsdesign.co.uk/
*/

/*  Copyright 2005-2006  Ricardo Galli Granada  (email : gallir@uib.es)
    Copyright 2007-2013 Donncha Ó Caoimh (http://ocaoimh.ie/) 
    Copyright 2014 Geraint Palmer (http://geraint.co/) and many others.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/**
* 
*/
class WpSuperCacheCdnMod
{
	// Plugin options storage key
	const OPTIONID = 'wp_super_cache_cdn_mod::options';
	const PAGEID = 'wpsupcachecdnmod';
	const TXTDOMAIN = 'wp-super-cache-cdn-mod';

	// Plugin Defaults
	private static $defaults = array(
		'enabled' => 0,
		'blog_url' => 'REPLACED WITH get_option("home") at INIT',
		'cdn_url' => 'http://cdn.example.co.uk',
		'exclude_string' => '.php',
		'include_string' => 'wp-content,wp-includes',
		'skip_https' => 1,
	);

	// On/Off switch
	public static $enabled;
	
	// Target Domain
	public static $blog_url;
	// Replacement Domain
	public static $cdn_url;
	
	// Includes Targets Search String
	public static $include_string;

	// Exclude Targets Search String
	public static $exclude_string;

	public static $skip_https;

	// Storage for exclusion list
	private static $array_excludes = array();


	/**
	 * Registers WpSuperCacheCdnMod::filter as output buffer, if needed.
	 */
	public static function init()
	{
		// Late init defaults
		if ( is_multisite() && !SUBDOMAIN_INSTALL ) {
			self::$defaults['blog_url'] = rtrim( 'http://'.DOMAIN_CURRENT_SITE.PATH_CURRENT_SITE , '/');
		}else{
			self::$defaults['blog_url'] = get_option('home');
		}

		self::populate_options();

		// Add filter when enabled
		if ( self::$enabled && is_plugin_active('wp-super-cache/wp-cache.php') ){	
			add_filter( 'wp_cache_ob_callback_filter', __CLASS__.'::filter' );
		}

		// Register the Admin Page
		add_action('admin_menu', __CLASS__.'::add_admin_page');
	}

	/**
	 * Populates the plugins options and settigns
	 */
	private static function populate_options()
	{	
		// Try and Fetch options from DB
		$args = get_option( self::OPTIONID, array() );
		
		// Parse together Defaults
		$args = wp_parse_args( $args, self::$defaults );

		// Init Class VARS
		self::$enabled  = $args['enabled'];
		self::$blog_url = $args['blog_url'];
		self::$cdn_url  = $args['cdn_url' ];
		self::$exclude_string = stripslashes($args['exclude_string']); 
		self::$include_string = stripslashes($args['include_string']); 
		self::$skip_https = $args['skip_https'];

		self::$array_excludes = array_map( 'trim', explode( ',', self::$exclude_string ) );
		//self::$include_string = str_replace("/", "\/", self::$include_string );

		//die($args['include_string'].self::$include_string.self::directories_regex_pattern());
	}

	public static function add_admin_page() {
		//global $wpmu_version;

		if ( function_exists( 'is_multisite' ) && is_multisite() && wpsupercache_site_admin() ) {
			add_submenu_page( 'ms-admin.php', 'WP Super Cache CDN Mod', 'WP Super Cache CDN Mod', 'manage_options', self::PAGEID, __CLASS__.'::settings_page' );
		}elseif
		// } elseif ( isset( $wpmu_version ) && wpsupercache_site_admin() ) {
		// 	add_submenu_page( 'wpmu-admin.php', 'WP Super Cache CDN Mod', 'WP Super Cache CDN Mod', 'manage_options', self::PAGEID, __CLASS__.'::settings_page' );
		// }

	//	if 
		( wpsupercache_site_admin() ) { // in single or MS mode add this menu item too, but only for superadmins in MS mode.
			add_submenu_page( 'CDN Super Cache Mod', 'WP Super Cache CDN Mod', 'manage_options', self::PAGEID, __CLASS__.'::settings_page' );
		}
	}


	/**
	 * Output filter which runs the actual plugin logic.
	 *
	 * Called by #wp_cache_ob_callback_filter
	 *
	 * @param String $content Content from OB
	 * @return String Filtered Content
	 */
	public static function filter( $content )
	{
		$ai1ec_render_js = get_query_var( 'ai1ec_render_js' ); 
		if (!empty( $ai1ec_render_js )) 
			return $content;

		// Sanity Check
		if ( self::$blog_url != self::$cdn_url ) {
			$dirs = self::directories_regex_pattern(); 

			$regex = '#(?<=[(\"\'])'.quotemeta( self::$blog_url ).'/(?:((?:'.$dirs.')[^\"\')]+)|([^/\"\']+\.[^/\"\')]+))(?=[\"\')])#';
			return preg_replace_callback( $regex, __CLASS__.'::rewriter', $content );
		}

		return $content;
	}

	/**
	 * Rewriter of URLs, used as replace-callback.
	 *
	 * Called by #WpSuperCacheCdnMod::filter.
	 *
	 * @param Array $match PREG match array.
	 * @return $String $match[0] or replacement url
	 */
	public static function rewriter( $match )
	{
		// Sanity Check for empty replacement 
		if ( self::$cdn_url == '' )
			return $match[0];

		// Skip if https && skip_https enabled
		if (self::$skip_https && substr($match[0], 0, 5) == 'https' )
			return $match[0];

		// Skip url if contains excluded string
		if ( self::exclude_match( $match[0], self::$array_excludes ) ) {
			return $match[0];
		} else {
			$dirs = self::directories_regex_pattern();
			if ( preg_match( '/' . $dirs . '/', $match[0] ) ) {	
				//$offset = scossdl_string_mod($match[1], count($arr_of_cnames));
				return str_replace( self::$blog_url, self::$cdn_url, $match[0] );
			} else {
				return $match[0];
			}
		}
	}

	/**
	 * Creates a regexp compatible pattern from the directories to be included in matching.
	 *
	 * @return String with the pattern with {@literal |} as prefix, or empty
	 */
	private static function directories_regex_pattern() 
	{
		$input = explode( ',', self::$include_string );
		if ( self::$include_string == '' || count($input) < 1 ) {
			return 'wp\-content|wp\-includes';
		} else {
			return str_replace( "/", "\/" , implode( '|', array_map( 'quotemeta', array_map( 'trim', $input ) ) ) );
		}
	}

	/**
	 * Determines whether to exclude a match.
	 *
	 * @param String $match URI to examine
	 * @param Array $excludes array of "badwords"
	 * @return Boolean true if to exclude given match from rewriting
	 */
	public static function exclude_match( $match, $excludes )
	{
		foreach ($excludes as $badword) 
		{
			if (stristr($match, $badword) != false) {
				return true;
			}
		}
		return false;
	}

	public static function settings_page($value='')
	{
		$valid_nonce = isset($_REQUEST['_wpnonce']) ? wp_verify_nonce($_REQUEST['_wpnonce'], 'wpsccm-wp-cache') : false;

		if ( $valid_nonce && isset( $_POST['action'] ) && ( $_POST['action'] == 'wpsccm_update_settings' ) )
		{	
			$args = get_option( self::OPTIONID, self::$defaults );

			$args['enabled'] = (int) $_POST['wpsccm_enabled'];
			$args['cdn_url'] = trim($_POST['wpsccm_cdn_url']);
			$args['include_string'] = trim($_POST['wpsccm_include_string']);
			$args['exclude_string'] = trim($_POST['wpsccm_exclude_string']);
			$args['skip_https'] = (int) $_POST['wpsccm_skip_https'];

	
			$args = wp_parse_args($args, self::$defaults);
			update_option( self::OPTIONID, $args );
			self::populate_options();
		}

		$example_cdn_uri = str_replace( 'http://', 'http://cdn.', str_replace( 'www.', '', get_option( 'home' ) ) );
		$example_cnames  = str_replace( 'http://cdn.', 'http://cdn1.', $example_cdn_uri );
		$example_cnames .= ',' . str_replace( 'http://cdn.', 'http://cdn2.', $example_cdn_uri );
		$example_cnames .= ',' . str_replace( 'http://cdn.', 'http://cdn3.', $example_cdn_uri );

		$example_cdn_uri = self::$cdn_url == get_option('siteurl') ? $example_cdn_uri : self::$cdn_url;
		$example_cdn_uri .= '/wp-includes/js/jquery/jquery.js';
		?>
		<a name="top"></a>
		<div class="wrap">
		<h2><?php  _e( 'WP Super Cache CDN Mod Settings', 'wp-super-cache-cdn-mod' ) ?></h2>
			<p><?php _e( 'This Mod is designed suppliment/replace the functionality of the default CDN tool built into WP Super Cache and over come its limitations.', 'wp-super-cache-cdn-mod' ); ?></p>
			<p><?php printf( __( 'This plugin lets you filter the Site address (URL) <code>"home"</code> instead of WordPress address (URL) <code>"site_url"</code> which makes WP Super Cache CDN functions compatable with having you wp-content DIR outside of the default WP director see: <a href="%s">Giving WordPress Its Own Directory</a>', 'wp-super-cache-cdn-mod') ,'https://codex.wordpress.org/Giving_WordPress_Its_Own_Directory' )?>
			
			<p><?php _e( '<strong style="color: BLUE">NOTE:</strong> This plugin is designed to operate independently of the built it CDN function of WP Super Cache, however WP Super Cache <strong>must</strong> be installed and activated for it to function', 'wp-super-cache-cdn-mod') ?></p>
			<p><?php printf( __( '<strong style="color: red">WARNING:</strong> Test some static urls e.g., %s  to ensure your CDN service is fully working before saving changes.', 'wp-super-cache' ), '<code>' . $example_cdn_uri . '</code>' ); ?></p>
			
			<p><form method="post" action="">
			<?php wp_nonce_field('wpsccm-wp-cache'); ?>
			<table class="form-table">
			<tbody>
				<tr valign="top">
					<td style='text-align: right'>
						<input id='wpsccm_enabled' type="checkbox" name="wpsccm_enabled" value="1" <?php checked( self::$enabled, 1 ) ?> />
					</td>
					<th scope="row"><label for="wpsccdnmod_enabled"><?php _e( 'Enable CDN Support', 'wp-super-cache' ); ?></label></th>
				</tr>
				<tr valign="top">
					<th scope="row"><label for="wpsccm_cdn_url"><?php _e( 'Off-site URL', 'wp-super-cache' ); ?></label></th>
					<td>
						<input type="text" name="wpsccm_cdn_url" value="<?php echo esc_url( self::$cdn_url ) ?>" size="64" class="regular-text code" /><br />
						<span class="description"><?php printf( __( 'The new URL to be used in place of %1$s for rewriting. No trailing <code>/</code> please.<br />Example: <code>%2$s</code>.', 'wp-super-cache' ), get_option( 'home' ), $example_cdn_uri ); ?></span>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><label for="wpsccm_include_string"><?php _e( 'Include directories', 'wp-super-cache' ); ?></label></th>
					<td>
						<input type="text" name="wpsccm_include_string" value="<?php echo esc_attr( self::$include_string ); ?>" size="64" class="regular-text code" /><br />
						<span class="description"><?php _e( 'Directories to include in static file matching. Use a comma as the delimiter. Default is <code>wp-content, wp-includes</code>, which will be enforced if this field is left empty.', 'wp-super-cache' ); ?></span>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row"><label for="wpsccm_exclude_string"><?php _e( 'Exclude if substring', 'wp-super-cache' ); ?></label></th>
					<td>
						<input type="text" name="wpsccm_exclude_string" value="<?php echo esc_attr( self::$exclude_string ); ?>" size="64" class="regular-text code" /><br />
						<span class="description"><?php _e( 'Excludes something from being rewritten if one of the above strings is found in the match. Use a comma as the delimiter like this, <code>.php, .flv, .do</code>, and always include <code>.php</code> (default).', 'wp-super-cache' ); ?></span>
					</td>
				</tr>
				<?php /*// ?>
				<!-- <tr valign="top">
					<th scope="row"><label for="ossdl_cname"><?php _e( 'Additional CNAMES', 'wp-super-cache' ); ?></label></th>
					<td>
						<input type="text" name="ossdl_cname" value="<?php echo esc_attr( get_option( 'ossdl_cname' ) ); ?>" size="64" class="regular-text code" /><br />
						<span class="description"><?php printf( __( 'These <a href="http://en.wikipedia.org/wiki/CNAME_record">CNAMES</a> will be used in place of %1$s for rewriting (in addition to the off-site URL above). Use a comma as the delimiter. For pages with a large number of static files, this can improve browser performance. CNAMEs may also need to be configured on your CDN.<br />Example: %2$s', 'wp-super-cache' ), get_option( 'siteurl' ), $example_cnames ); ?></span>
					</td>
				</tr> -->
				<?php //*/ ?>
				<tr valign="top">
					<th scope="row" colspan='2'><label><input id="wpsccm_skip_https" type='checkbox' name='wpsccm_skip_https' value='1' <?php checked( self::$skip_https, 1 ) ?> /> <?php _e( 'Skip https URLs to avoid "mixed content" errors', 'wp-super-cache' ); ?></label></th>
				</tr>
			</tbody></table>
			<input type="hidden" name="action" value="wpsccm_update_settings" />
			<p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" /></p>
			</form></p>
			<p><?php _e( 'CDN functionality provided by <a href="http://wordpress.org/extend/plugins/ossdl-cdn-off-linker/">OSSDL CDN Off Linker</a> by <a href="http://mark.ossdl.de/">Mark Kubacki</a>', 'wp-super-cache' ); ?></p>
			</div>
		<?php
	}
}

add_action('init', 'WpSuperCacheCdnMod::init');