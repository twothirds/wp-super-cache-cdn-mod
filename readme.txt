=== WP Super Cache CDN mod ===
Contributors: geraint
Tags: performance,caching,wp-cache,wp-super-cache,cache, cdn, cloudfront
Requires at least: 3.0
Tested up to: 3.9
Stable tag: 1.2
License: GPLv2 or later

Widget Logic lets you control on which pages widgets appear using WP's conditional tags. It also adds a 'widget_content' filter.

== Description ==
This plugin gives every widget an extra control field called "Widget logic" that lets you control the pages that the widget will appear on. The text field lets you use WP's [Conditional Tags](http://codex.wordpress.org/Conditional_Tags), or any general PHP code.

== Installation ==

1. Upload `wp-super-cache-cdn-mod.php` to the `/wp-content/plugins/` directory
2. Ensure to install and activate `WP Super Cache` first.
3. Activate the plugin through the 'Plugins' menu in WordPress
4. That's it. The configuring and options are in the usual widget admin interface.

= Configuration =


== FAQ ==

= Why isn't it working? =

Try switching to the WP default theme - if the problem goes away, there is something specific to your theme that may be interfering with the WP conditional tags.

The most common sources of problems are:

* The logic text on one of your widgets is invalid PHP
* Your theme performs custom queries before calling the dynamic sidebar -- if so, try ticking the `wp_reset_query` option.

== Change Log ==

= 1.2 =
* fix: Admin area plugin confict with `ai1ec_js`

= 1.1 =
* fix: Add Logic to prevent activation if `WP-Super-Cache` isn't installed and activated.
* new: Multi site support and folder path includes "year1/files" etc.

= 1.0 =  
* Private beta release